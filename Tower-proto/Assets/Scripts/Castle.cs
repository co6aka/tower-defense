﻿using UnityEngine;

public class Castle : MonoBehaviour
{
    Health health;

    private void Start()
    {
        health = GetComponentInChildren<Health>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        // if intruder enters castle, health is down
        if (collision.gameObject.tag == "Intruder")
        {
            health.Decrease();
            Destroy(collision.gameObject);
        }
    }
}
