﻿using UnityEngine;
using UnityEngine.AI;

public class Intruder : MonoBehaviour {

    Health health;

    // Use this for initialization
    void Start () {
        // Navigate to Castle
        GameObject castle = GameObject.Find("Castle");
        if (castle)
            GetComponent<NavMeshAgent>().destination = castle.transform.position;

        health = GetComponentInChildren<Health>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Cannonball")
            health.Decrease();
    }
}
