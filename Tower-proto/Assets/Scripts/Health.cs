﻿using UnityEngine;

public class Health : MonoBehaviour {

    // The TextMesh Component
    TextMesh tm;

    void Start () {
        tm = GetComponent<TextMesh>();
    }
    
    void Update () {
        transform.forward = Camera.main.transform.forward;   // Face the Camera
    }
   
    // drop one notch down
    public void Decrease() {

		if (tm.text.Length > 1)  // count the dashes
			tm.text = tm.text.Remove (tm.text.Length - 1);
		else 	
			Destroy (transform.parent.gameObject);   // kill the parent object we're attached to
    }
}
